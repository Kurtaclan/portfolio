import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom'
// eslint-disable-next-line
import { Navbar, NavItem, Nav, MenuItem, NavDropdown, Row, Col } from 'react-bootstrap';
import work from './Projects/Work.json';
import { Unity } from 'react-unity-webgl';
import Scrollspy from 'react-scrollspy'

class App extends Component {
    render() {
        return (
            <div>
                <Router>
                    <div>
                        <Route path="/" component={navBarMenu} />
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/Demo/Arcade" component={Arcade} />
                            <Route path="/Demo/NasaSpace" component={NasaSpace} />
                            <Route path="/Demo/SpaceShooter" component={SpaceShooter} />
                            <Route path="/Demo/PointAndClick" component={PointAndClick} />
                        </Switch>
                    </div>
                </Router>
            </div>
        );
    }
}

const navBarMenu = () => (
    <Navbar inverse fixedTop collapseOnSelect className='navbar-custom'>
        <Navbar.Header>
            <Navbar.Brand>
                <Link to="/">Travis Peterson</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Scrollspy className='nav navbar-nav navbar-right' items={['Portfolio', 'Resume', 'Other']} currentClassName="active">
                <li role="presentation"><a href='#Portfolio'>Portfolio</a></li>
                <li role="presentation"><a href='#Resume'>Resume</a></li>
                <li role="presentation"><a href='#Other'>Other Work</a></li>
            </Scrollspy>
        </Navbar.Collapse>
    </Navbar>
);

var professionalProjects = work.Professional_Work.map((data, i) => {
    return (<ProjectView key={i} index={i} linkUrl={data.Url} repoUrl={data.RepoUrl} description={data.Description} title={data.Title} image={data.Image} />);
});
var hackathons = work.Hackathons.map((data, i) => {
    return (<ProjectView key={i} index={i} linkUrl={data.Url} repoUrl={data.RepoUrl} description={data.Description} title={data.Title} image={data.Image} />);
});
var weekendProjects = work.Weekend_Projects.map((data, i) => {
    return (<ProjectView key={i} index={i} linkUrl={data.Url} repoUrl={data.RepoUrl} description={data.Description} title={data.Title} image={data.Image} />);
});

const Home = () => (
    <div>
        <header>
            <div className="container">
                <Row>
                    <Col>
                        <figure>
                            <img className="img-circle img-responsive img-width" alt="Travis Peterson Headshot" src="/Images/Headshot.jpg" />
                        </figure>
                        <div className="intro-text">
                            <span className="name">Travis Peterson</span>
                            <hr className="star-light" />
                            <span className="skills">Web Developer - Full-Stack Engineer - Team Leader</span>
                            <br />
                            <span className="skills">C# - Javascript - PostgreSQL - SQL - MySQL - AWS - Azure</span>
                        </div>
                        <Row>
                            <Col xs={8} xsOffset={2} md={6} mdOffset={3} lg={4} lgOffset={4} className="social-btns">
                                <Col xs={3} className="social-btn-holder">
                                    <a href="https://plus.google.com/113125453866253102310" target="_blank" rel="noopener noreferrer" className="btn btn-social btn-block btn-google">
                                        <i className="fa fa-google"></i>
                                    </a>
                                </Col>

                                <Col xs={3} className="social-btn-holder">
                                    <a href="https://www.linkedin.com/in/travispeterson04" target="_blank" rel="noopener noreferrer" className="btn btn-social btn-block btn-linkedin">
                                        <i className="fa fa-linkedin"></i>
                                    </a>
                                </Col>

                                <Col xs={3} className="social-btn-holder">
                                    <a href="https://github.com/Kurtaclan" target="_blank" rel="noopener noreferrer" className="btn btn-social btn-block btn-github">
                                        <i className="fa fa-github"></i>
                                    </a>
                                </Col>
                                <Col xs={3} className="social-btn-holder">
                                    <a href="https://bitbucket.org/Kurtaclan/" target="_blank" rel="noopener noreferrer" className="btn btn-social btn-block btn-linkedin">
                                        <i className="fa fa-bitbucket"></i>
                                    </a>
                                </Col>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        </header>
        <section id='Portfolio'>
            <div className="container">
                <Row>
                    <Col className="text-center">
                        <h2>Portfolio</h2>
                        <hr className="star-primary" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {professionalProjects}
                    </Col>
                </Row>
            </div>
        </section>
        <section id='Resume' className='success'>
            <Resume />
        </section>
        <section id='Other'>
            <div className="container">
                <Row>
                    <Col className="text-center">
                        <h2>Hackathons</h2>
                        <hr className="star-primary" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {hackathons}
                    </Col>
                </Row>
            </div>
        </section>
        <section className='success'>
            <div className="container">
                <Row>
                    <Col className="text-center">
                        <h2>Weekend Projects</h2>
                        <hr className="star-light" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {weekendProjects}
                    </Col>
                </Row>
            </div>
        </section>
    </div>
)

const Resume = () => (
    <div className="container">
        <Row>
            <Col sm={12}>
                <div className="resume">
                    <Row>
                        <Col className="text-center">
                            <h2>Resume</h2>
                            <hr className="star-light" />
                            <a target="_blank" className='btn btn-lg btn-outline' href='/Download/TravisPetersonResume.docx'><i className="fa fa-file-o"></i> Download</a>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <ul>
                                <li>
                                    <div className="bs-callout bs-callout-danger">
                                        <h4 className="list-group-item-heading">
                                            Juristerra, Legal Startup, Senior Developer, November 2017-Present
                                            </h4>
                                        <div className="list-group-item-text">
                                            <p>
                                                <strong>
                                                    Team Leader
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Managed team of five Jr Developers, assigned tasks and assisted with learning new technologies.
                                                    </li>
                                                </ul>
                                            </p>
                                            <p>
                                                <strong>
                                                    Front-end Design (PHP, Node.Js, React, Jquery, AWS)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Updated and supported existing PHP frontend.
                                                    </li>
                                                    <li>
                                                        Designed and built new React frontend.
                                                    </li>
                                                </ul>
                                            </p>
                                            <p>
                                                <strong>
                                                    Back-end Architecture (PHP, Express, Node.Js, MySql, PostgreSql, AWS)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Maintained and updated existing databases and apis in PHP and MySql.
                                                    </li>
                                                    <li>
                                                        Created new db schema for site redesign.
                                                    </li>
                                                    <li>
                                                        Created new restful backend api using Express and PostgreSql
                                                    </li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="bs-callout bs-callout-danger">
                                        <h4 className="list-group-item-heading">
                                            AR Net, Online Arrest Records Processing, Full Stack Developer, August 2017-November 2017
                                            </h4>
                                        <div className="list-group-item-text">
                                            <p>
                                                <strong>
                                                    Front-end Design (.Net Core MVC, JQuery, Azure)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Created front end to allow users to setup the parameters and jurisdictions of arrest records they wish to see.
                                                    </li>
                                                    <li>
                                                        Created admin portal to manage users and law enforcment integrations.
                                                    </li>
                                                </ul>
                                            </p>
                                            <p>
                                                <strong>
                                                    Back-end Architecture (.Net Core, Sql, Azure)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Created databases to store and manage user data, as well as converting existing db.
                                                    </li>
                                                    <li>
                                                        Created api that connected to law enforcment agencies to download and import arrest records.
                                                    </li>
                                                    <li>
                                                        Created api to send scheduled reports on user's configured arrest alerts.
                                                    </li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="bs-callout bs-callout-danger">
                                        <h4 className="list-group-item-heading">
                                            Redulo, Education Startup, Full Stack Developer, September 2016 - May 2017
                                            </h4>
                                        <div className="list-group-item-text">
                                            <p>
                                                <strong>
                                                    Front-end Design (React, AWS)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Created student and teacher portals to purchase and create lessons.
                                                    </li>
                                                </ul>
                                            </p>
                                            <p>
                                                <strong>
                                                    Back-end Architecture (Node.Js, Express, MongoDB, AWS)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Created databases to store and manage student data.
                                                    </li>
                                                    <li>
                                                        Created secure payment system and scheduling system for students and teachers.
                                                    </li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="bs-callout bs-callout-danger">
                                        <h4 className="list-group-item-heading">
                                            Software Developer at Ethos Lending, Software Engineer, January 2014-June 2016
                                        </h4>
                                        <div className="list-group-item-text">
                                            <p>
                                                <strong>
                                                    Front-end Design (.Net MVC, HTML5, Jquery, Azure)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Created three user-facing web portals with account creation flows and processes; to allow different users to participate in the creation and submittal of loans.
                                                    </li>
                                                    <li>
                                                        Designed five user dashboards to present and summarize information based upon the user�s rights and current view.
                                                    </li>
                                                    <li>
                                                        Created a document splitting utility to allow users to select and group thumbnail images to create PDFs.
                                                    </li>
                                                    <li>
                                                        Created web-based ticketing system for clients to interact with the sales team.
                                                    </li>
                                                </ul>
                                            </p>
                                            <p>
                                                <strong>
                                                    Back-end Architecture (.Net 4.5, Sql, Azure)
                                                </strong>
                                                <ul>
                                                    <li>
                                                        Created PDF manipulation service to create thumbnails from pdfs as well as split, compress, and merge functionality.
                                                    </li>
                                                    <li>
                                                        Integrated with Optimal Blue and Experian utilizing SOAP calls to their web services.
                                                    </li>
                                                    <li>
                                                        Created Calendar-based task scheduling framework.
                                                    </li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </div>
            </Col>
        </Row>
    </div>
)

const Arcade = () => (
    <div className="app" >
        <h2>Arcade Classics</h2>
        <Unity src="/Release/Arcade.json" />
    </div>
)

const NasaSpace = () => (
    <div className="app">
        <h2>Nasa Space Apps Challenge 2016</h2>
        <Unity src="/Release/Nasa.json" />
    </div>
)

const SpaceShooter = () => (
    <div className="app">
        <h2>Space Shooter</h2>
        <Unity src="/Release/SpaceTest.json" />
    </div>
)

const PointAndClick = () => (
    <div className="app">
        <h2>Point and Click Puzzle Game</h2>
        <Unity src="/Release/PointAndClick.json" />
    </div>
)

function ProjectView(props) {
    return (
        <Row className='project'>
            {props.index % 2 === 0 ? '' : <Col md={6} xsHidden smHidden>
                <figure>
                    <img className="img-full" alt={props.title} src={"/Images/" + props.image} ></img>
                </figure>
            </Col>}
            <Col md={6} className='description'>
                <h2>{props.title}</h2>
                <p>{props.description}</p>
                <h3>
                    {
                        props.linkUrl && (
                            <a target="_blank" className='btn btn-lg btn-outline' href={props.linkUrl}><i className="fa fa-link"></i> Link</a>
                        )
                    }
                    {
                        props.repoUrl && (
                            <a target="_blank" className='btn btn-lg btn-outline' href={props.repoUrl}><i className="fa fa-bitbucket"></i> Repo</a>
                        )
                    }
                </h3>
            </Col>
            {props.index % 2 !== 0 ? '' : <Col md={6} xsHidden smHidden>
                <figure>
                    <img className="img-full" alt={props.title} src={"/Images/" + props.image} ></img>
                </figure>
            </Col>}
            <Col md={6} mdHidden lgHidden>
                <figure>
                    <img className="img-full" alt={props.title} src={"/Images/" + props.image} ></img>
                </figure>
            </Col>
        </Row>
    );
}

export default App;